# Generated by Django 3.1.4 on 2021-01-03 00:15

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('rover', '0007_auto_20210103_0530'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='dag_task',
            options={'ordering': ['-updated_at_ts'], 'verbose_name': 'Dag Task', 'verbose_name_plural': 'Dag Tasks'},
        ),
        migrations.AlterModelOptions(
            name='files',
            options={'ordering': ['-created_ts'], 'verbose_name': 'Files', 'verbose_name_plural': 'Files'},
        ),
        migrations.AlterModelOptions(
            name='historicaldag_task',
            options={'get_latest_by': 'history_date', 'ordering': ('-history_date', '-history_id'), 'verbose_name': 'historical Dag Task'},
        ),
        migrations.AlterModelOptions(
            name='historicalfiles',
            options={'get_latest_by': 'history_date', 'ordering': ('-history_date', '-history_id'), 'verbose_name': 'historical Files'},
        ),
    ]
