from django.contrib import admin
from .models import DAG, Dag_File, Dag_Task, Files

# Register your models here.

@admin.register(DAG)
class DAG_Admin(admin.ModelAdmin):
    list_display = ('name', )

    def has_delete_permission(self, request, obj=None):
        #Disable delete
        return False

@admin.register(Dag_File)
class DAG_File_Admin(admin.ModelAdmin):
    list_display = ('name', )

    def has_delete_permission(self, request, obj=None):
        #Disable delete
        return False

class FilesTabular(admin.TabularInline):
    model = Files

@admin.register(Dag_Task)
class DagTaskAdmin(admin.ModelAdmin):
    inlines = [FilesTabular]
    list_display = ('dag', 'execute_at_ts', 'created_ts', 'updated_at_ts', )

    def has_delete_permission(self, request, obj=None):
        #Disable delete
        return False


@admin.register(Files)
class FilesAdmin(admin.ModelAdmin):
    list_display = ('dag_task', 'dag_file', 'path', 'created_ts', )

    def has_delete_permission(self, request, obj=None):
        #Disable delete
        return False