from django import forms
from .models import *
from django.forms.models import inlineformset_factory

class DagTaskFilesCreateForm(forms.ModelForm):
    
    class Meta:
        model = Files
        exclude = ()

DagTaskFilesCreateFormSet = inlineformset_factory(
    Dag_Task # parent
    , Files # child
    , form = DagTaskFilesCreateForm
    , extra = 1
    , can_delete = False
)
