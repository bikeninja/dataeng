from django.db import models
from django.utils import timezone
from simple_history.models import HistoricalRecords
from django.utils.deconstruct import deconstructible
from django.core.exceptions import ValidationError
import os
from datetime import datetime

# Create your models here.

@deconstructible
class PathAndRename():
# https://stackoverflow.com/questions/25767787/django-cannot-create-migrations-for-imagefield-with-dynamic-upload-to-value
    def __init__(self, path, prefix):
        self.path = path
        self.prefix = prefix
    def __call__(self, instance, filename):
        ext = filename.split('.')[-1]
        filename = f'{self.prefix}/{instance.dag_file}/{datetime.now():%Y%m%d%H%M%S}.{ext}'
        return os.path.join(self.path, filename)

def validate_for_csvfile_type(value):
    # https://stackoverflow.com/questions/6460848/how-to-limit-file-types-on-file-uploads-for-modelforms-with-filefields
    # print(value.file.content_type)
    accepted_file_types = ['application/vnd.ms-excel', 'text/plain', 'text/x-csv']
    if value.file.content_type not in accepted_file_types:
        raise ValidationError(f'content type of file has to be either of {",".join(accepted_file_types)}')

path_and_rename = PathAndRename('rover', 'dags')

class DAG(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=100)
    file_list = models.ManyToManyField('Dag_File')

    def __str__(self):
        return self.name
    
    class Meta:
        verbose_name = "DAG"
        verbose_name_plural = "DAGS"

class Dag_File(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=100)

    def __str__(self):
        return self.name
    
    class Meta:
        verbose_name = "Dag File"
        verbose_name_plural = "Dag Files"

class Dag_Task(models.Model):
    id = models.AutoField(primary_key=True)
    dag = models.ForeignKey('DAG', on_delete=models.SET_NULL, null=True)
    execute_at_ts = models.DateTimeField()
    parameter = models.CharField(max_length=100, null=True, blank=True)
    created_ts = models.DateTimeField(auto_now_add=True)
    executed_at_ts = models.DateTimeField(null=True, blank=True)
    updated_at_ts = models.DateTimeField(auto_now=True)
    history = HistoricalRecords()

    def __str__(self):
        return f'{self.id}'
    
    class Meta:
        verbose_name = "Dag Task"
        verbose_name_plural = "Dag Tasks"
        ordering = ['-updated_at_ts']
        


class Files(models.Model):
    id = models.AutoField(primary_key=True)
    dag_task = models.ForeignKey('Dag_Task', on_delete=models.SET_NULL, null=True)
    dag_file = models.ForeignKey('Dag_File', on_delete=models.SET_NULL, null=True)
    path = models.FileField(upload_to=path_and_rename, validators=[validate_for_csvfile_type])
    created_ts = models.DateTimeField(auto_now_add=True, null=True, blank=True)
    history = HistoricalRecords()

    def __str__(self):
        return f'{self.path}'
    
    class Meta:
        verbose_name = "Files"
        verbose_name_plural = "Files"
        ordering = ['-created_ts']

class Dag_Runs(models.Model):
    id = models.AutoField(primary_key=True)
    dag_task = models.ForeignKey(Dag_Task, on_delete=models.SET_NULL, null=True, blank=True)
    created_ts = models.DateTimeField(auto_now_add=True, null=True, blank=True)
    airflow_dag_run_id = models.CharField(max_length=100, null=True, blank=True)
    airflow_dag_run_status = models.CharField(max_length=100, null=True, blank=True)
    message = models.CharField(max_length=1000, null=True, blank=True)
    history = HistoricalRecords()

    class Meta:
        ordering = ['-created_ts']
