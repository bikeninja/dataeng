import requests
import base64

Airflow_URL = 'http://13.126.191.137:8080/api/v1/'

def get_credentials():
    
    credentials = ('admin:admin').encode('utf-8')
    base64_encoded_credentials  = base64.b64encode(credentials).decode('utf-8')
    headers = {
        'Authorization': 'Basic ' + base64_encoded_credentials
        , 'content-type': 'application/json'
    }

    return headers

def trigger_dag(dag, conf):
    headers = get_credentials()
    airflow_resp = requests.post(
            Airflow_URL + 'dags/' + dag + '/dagRuns'
            , json={"conf": conf}
            , headers=headers
        )
    return airflow_resp


def get_dagRun_status(dagName, dag_run_id):
    headers = get_credentials()
    airflow_resp = requests.get(
            Airflow_URL + 'dags/' + dagName + '/dagRuns/' + dag_run_id
            , headers=headers
        )
    return airflow_resp