from django.urls import path
from .views import *

urlpatterns = [
    path('dagtask/', DagTask.as_view(), name='dagtask')
    , path('dagtask/create/', DagTaskCreate.as_view(), name='dagtask_create')
    , path('dagtask/<pk>/', DagTaskView.as_view(), name='dagtask_view')
    , path('dagtask/<int:pk>/push/', dagTaskPush, name='dagtask_refresh')
    , path('dagrun/<int:pk>/status/', dagTaskStatus, name='dagtask_status')
]