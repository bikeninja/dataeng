from django.forms.utils import ErrorList
import requests
from .models import *
from .forms import *
from django.views.generic.edit import CreateView
from django.views.generic import ListView, DetailView
from django.shortcuts import get_object_or_404, redirect
from django.urls import reverse_lazy, reverse
from django.db import transaction
from datetime import datetime
from django.conf import settings
from django.utils import timezone
from .widgets import BootstrapDateTimePickerInput
from django.core import serializers
import json
from django.http import JsonResponse, HttpResponseRedirect
from requests.auth import HTTPBasicAuth 
from .airflow import trigger_dag, get_dagRun_status


def dagTaskPush(request, pk):
    if request.method == 'GET':
        dagTask = Dag_Task.objects.get(id=pk)
        dagFile = Files.objects.filter(dag_task_id = pk)

        file_data = {}
        for file in dagFile:
            prefix = 'https://' if request.is_secure() else 'http://'
            file_url = prefix + request.get_host() + file.path.url
            file_data[file.dag_file.name] = file_url

        if dagTask.parameter:
            parameter = json.loads(dagTask.parameter)
        else:
            parameter = {}
        
        conf = {
                "parameters": parameter
                , "files": file_data
            }
        
        airflow_resp = trigger_dag(dagTask.dag.name, conf)
        try:
            airflow_resp.raise_for_status()
            data = airflow_resp.json()
            Dag_Runs.objects.create(dag_task_id=pk, airflow_dag_run_status=data['state'], airflow_dag_run_id=data['dag_run_id'])
        except Exception:
            Dag_Runs.objects.create(dag_task_id=pk, message='Failed to trigger dag')

        return redirect('dagtask_view', pk=pk)

def dagTaskStatus(request, pk):
    if request.method == 'GET':
        dagRun = Dag_Runs.objects.get(id=pk)
        dagTask = Dag_Task.objects.get(pk=dagRun.dag_task_id)
        dagName = dagTask.dag.name
        dag_run_id = dagRun.airflow_dag_run_id

        r = get_dagRun_status(dagName, dag_run_id)

        try:
            data = r.json()
            Dag_Runs.objects.filter(pk=pk).update(airflow_dag_run_status=data['state'])
        except Exception as e:
            print(r.status_code, r.content, e)

        return redirect('dagtask_view', pk=dagTask.id)


class DagTask(ListView):
    model = Dag_Task
    template_name = 'rover/dag_task.html'
    context_object_name = 'dag_task'

class DagTaskView(DetailView):
    model = Dag_Task
    template_name = 'rover/dag_task_detail.html'   
    context_object_name = 'dag_task' 
    def get_context_data(self, **kwargs):
        data = super(DagTaskView, self).get_context_data(**kwargs)
        pk = self.kwargs['pk']
        data['files'] = Files.objects.filter(dag_task_id = pk )
        data['dag_runs'] = Dag_Runs.objects.filter(dag_task_id = pk )
        return data


class DagTaskCreate(CreateView):
    # https://medium.com/@adandan01/django-inline-formsets-example-mybook-420cc4b6225d
    model = Dag_Task
    template_name = 'rover/dag_task_form.html'
    fields = ['dag', 'execute_at_ts', 'parameter']

    def get_context_data(self, **kwargs):
        data = super(DagTaskCreate, self).get_context_data(**kwargs)
        if self.request.POST:
            data['dagtaskfiles'] = DagTaskFilesCreateFormSet(self.request.POST
                , self.request.FILES
                , instance=self.object)
        else:
            data['additional_data'] = json.dumps({
                "dag_file": serializers.serialize('json', DAG.objects.all())
                , "files": serializers.serialize('json', Dag_File.objects.all())
            })
            data['dagtaskfiles'] = DagTaskFilesCreateFormSet()
            # print(data)
        return data
    
    def form_valid(self, form):
        context = self.get_context_data()
        dagtaskfiles = context['dagtaskfiles']
        with transaction.atomic():
            self.object = form.save()
            if dagtaskfiles.is_valid():
                dagtaskfiles.instance = self.object
                dagtaskfiles.save()
        return super(DagTaskCreate, self).form_valid(form)
    
    def get_form(self, form_class=None):
        form = super(DagTaskCreate, self).get_form(form_class)
        form.fields['execute_at_ts'].widget = forms.DateTimeInput(
            attrs={'class':'datepicker', 'value': datetime.now().strftime('%Y-%m-%d %H:%M:%S')}
        )
        return form
    
    def get_success_url(self):
        return reverse('dagtask_view', kwargs={'pk': self.object.pk})
