from django import forms
 
from .models import DataEng

class DataEngForm(forms.ModelForm):
    class Meta:
        model = DataEng
        fields = ['id', 'task', 'params', 'attachment']