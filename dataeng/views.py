from django.shortcuts import render, redirect
from django.views.generic import ListView, View
from django.http import JsonResponse
from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.templatetags.static import static
import requests
from django.conf import settings
import json
from django.utils import timezone
from datetime import datetime
from django.http import JsonResponse

from .forms import DataEngForm
from .models import DataEng, TaskName

HBB_CENTRAL_URL = settings.HBB_CENTRAL_URL

# Create your views here.
class TasksView(LoginRequiredMixin, ListView):
    login_url = '/accounts/login/'
    redirect_field_name = 'redirect_to'
    model = DataEng
    template_name = 'dataeng/tasks.html'
    context_object_name = 'tasks'

@login_required
def createTask(request):
    if request.method == 'POST':
        form = DataEngForm(request.POST, request.FILES)
        if form.is_valid():
            form.save()
            return redirect('tasks')
    else:
        form = DataEngForm()
    return render(request, 'dataeng/upload.html', {'form': form})

@login_required
def refreshTask(request, id):
    if request.method == 'GET':
        task = DataEng.objects.get(id=id)
        task_name = task.task.task_name
        params = task.params
        attachment = task.attachment
        refreshed = task.refreshed_at
        task_id = task.pk

        request_data = {}

        if params:
            args = json.loads(params) 
        else:
            args = {}

        if task_name:
            request_data['task'] = task_name
        
        if attachment:
            prefix = 'https://' if request.is_secure() else 'http://'
            file_url = prefix + request.get_host() + attachment.url
            args['file_path'] = file_url

        request_data['args'] = args

        r = requests.post(HBB_CENTRAL_URL + 'runtask', json=request_data)

        if r.status_code != 200:
            message = r.content
            print(message)
        else:
            data = r.json()
            task.task_scheduled_id = data.get('task_id', None)
            message = 'added to queue'        

        task.message = message
        task.refreshed_at = datetime.now(tz=timezone.utc)
        task.save()

    return redirect('tasks')

@login_required
def refreshTaskStatus(request, id):
    if request.method == 'GET':
        task = DataEng.objects.get(id=id)
        task_scheduled_id = task.task_scheduled_id
        r = requests.get(HBB_CENTRAL_URL + 'runtask/' + task_scheduled_id)

        try:
            data = r.json()
            task_scheduled_status = data.get('message', None)
        except:        
            task_scheduled_status = r.content
        
        task.task_scheduled_status = task_scheduled_status
        task.save()

        # TODO: Create end point in hbb_central to run the tasks, save log and refreshed_at in table
    return redirect('tasks')