from django.db import models
from datetime import datetime
from django.utils import timezone
from django.core.exceptions import ValidationError
import os
from django.utils.deconstruct import deconstructible
from simple_history.models import HistoricalRecords

# Create your models here.

@deconstructible
class PathAndRename(object):
    # https://stackoverflow.com/questions/25767787/django-cannot-create-migrations-for-imagefield-with-dynamic-upload-to-value
    def __init__(self, path, prefix):
        self.path = path
        self.prefix = prefix
    def __call__(self, instance, filename):
        ext = filename.split('.')[-1]
        filename = f'{self.prefix}/{instance.task}/{datetime.now():%Y%m%d%H%M%S}.{ext}'
        return os.path.join(self.path, filename)

def validate_for_csvfile_type(value):
    # https://stackoverflow.com/questions/6460848/how-to-limit-file-types-on-file-uploads-for-modelforms-with-filefields
    # print(value.file.content_type)
    accepted_file_types = ['application/vnd.ms-excel', 'text/plain', 'text/x-csv']
    if value.file.content_type not in accepted_file_types:
        raise ValidationError(f'content type of file has to be either of {",".join(accepted_file_types)}')

path_and_rename = PathAndRename('dataeng', 'tasks')

class DataEng(models.Model):
    id = models.AutoField(primary_key=True)
    task = models.ForeignKey('TaskName', on_delete=models.SET_NULL, null=True)
    params = models.CharField(max_length=100)
    attachment = models.FileField(upload_to=path_and_rename, validators=[validate_for_csvfile_type])
    # pushed = models.BooleanField(default=False)
    created_at = models.DateTimeField(default=timezone.now)
    refreshed_at = models.DateTimeField(null=True, blank=True)
    message = models.CharField(max_length=500, null=True, blank=True)
    task_scheduled_id = models.CharField(max_length=100, null=True, blank=True)
    task_scheduled_status = models.CharField(max_length=500, null=True, blank=True)
    history = HistoricalRecords()


    def __str__(self):
        return f"{self.task}, {self.params}"

    class Meta:
        verbose_name = "Task Upload"
        verbose_name_plural = "Task Uploads"
        ordering = ['-created_at']

    def delete(self, *args, **kwargs):
        self.attachment.delete()
        super().delete(*args, **kwargs)

class TaskName(models.Model):
    id = models.AutoField(primary_key=True)
    task_name = models.CharField(max_length=100)
    created_ts = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.task_name
    
    class Mets:
        verbose_name = "Task Name"
        verbose_name_plural = "Task Names"


