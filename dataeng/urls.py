from django.urls import path
from .views import TasksView, createTask, refreshTask, refreshTaskStatus

urlpatterns = [
    path('', TasksView.as_view(), name='tasks')
    , path('<int:id>/push', refreshTask, name='task_push')
    , path('<int:id>/status', refreshTaskStatus, name='task_status')
]