from django.contrib import admin
from django import forms
from .models import DataEng, TaskName
from adminsortable2.admin import SortableAdminMixin
# Register your models here.

@admin.register(TaskName)
class TaskNameAdmin(admin.ModelAdmin):
    list_display = ('task_name', )

    def has_delete_permission(self, request, obj=None):
        #Disable delete
        return False

@admin.register(DataEng)
class DataEngAdmin(admin.ModelAdmin):
    list_display = ('created_at', 'task', 'params', 'refreshed_at', )

    def view_pushed(self, obj):
        return obj.pushed

    view_pushed.short_description = "Pushed"

    def has_delete_permission(self, request, obj=None):
        #Disable delete
        return False

    def get_readonly_fields(self, request, obj=None):
        if obj:
            return [field.name for field in DataEng._meta.get_fields() if field.name != 'id' ]
        else:
            return ['created_at', 'refreshed_at']


            # :TODO: Call Python function on click, get response and update on db
