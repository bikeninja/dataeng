from .base import *
import os

dev_setting = os.getenv('DATAENG_DEV')
if dev_setting:
    from .dev import *
else:
    from .prod import *